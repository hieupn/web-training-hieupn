# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table category_table (
  category_id                   integer auto_increment not null,
  category_name                 varchar(255),
  category_note                 varchar(255),
  constraint pk_category_table primary key (category_id)
);

create table user_table (
  user_id                       integer auto_increment not null,
  profile_path                  varchar(255),
  user_name                     varchar(255),
  user_tel                      varchar(255),
  user_mail                     varchar(255),
  user_note                     varchar(255),
  user_password                 varchar(255),
  category_id                   integer,
  constraint pk_user_table primary key (user_id)
);

alter table user_table add constraint fk_user_table_category_id foreign key (category_id) references category_table (category_id) on delete restrict on update restrict;
create index ix_user_table_category_id on user_table (category_id);


# --- !Downs

alter table user_table drop foreign key fk_user_table_category_id;
drop index ix_user_table_category_id on user_table;

drop table if exists category_table;

drop table if exists user_table;

