package services.utils;

import java.io.File;
import java.util.Calendar;

import play.mvc.Http.MultipartFormData.FilePart;

public class Utils {
	public static String makeUploadFileName(FilePart<File> file){
		
//		Long stamp = System.currentTimeMillis();	/*Cach nay cung on*/
		String fName = file.getFilename();
		String fileName;
		if(fName.equals(null)||fName.equals("")){
			fileName = "###";
		}
		else {
			Long stamp = Calendar.getInstance().getTimeInMillis();
			fileName = stamp.toString();
		}
		
		return fileName;
	}
}
