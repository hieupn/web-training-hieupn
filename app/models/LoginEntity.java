package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;


@Entity
@Table(name = "user_table")

public class LoginEntity extends Model { 
	


		@Id
		private Integer	userId;
		private String	userPassword;
		
	
		
		public LoginEntity() {
			super();
		}
		
		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		}


		public String getUserPassword() {
			return userPassword;
		}


		public void setUserPassword(String userPassword) {
			this.userPassword = userPassword;
		}

		public static Finder<Long, LoginEntity> find = new Finder<Long, LoginEntity>(LoginEntity.class);
	
}
