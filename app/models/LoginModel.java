package models;

public class LoginModel {
	
	public Integer loginId;
	public String loginPass;
	
	public LoginModel() {
		super();
	}
	
	public LoginModel(LoginEntity db) {
		super();
		this.loginId = db.getUserId();
		this.loginPass = db.getUserPassword();
	}

	public Integer getLoginId() {
		return loginId;
	}

	public void setLoginName(Integer loginId) {
		this.loginId = loginId;
	}

	public String getLoginPass() {
		return loginPass;
	}

	public void setLoginPass(String loginPass) {
		this.loginPass = loginPass;
	}

}
