package models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.avaje.ebean.Model;

@Entity
@Table(name = "user_table")
public class UserEntity extends Model {

	@Id
	private Integer	userId;
	
	private String	profilePath;
	private String	userName;
	private String	userTel;
	private String	userMail;
	private String	userNote;
	private String	userPassword;
	
	@ManyToOne
	@JoinColumn(name = "category_id", referencedColumnName="category_id")
	public CategoryEntity category;
	
	public UserEntity() {
		super();
	}

		
	public String getProfilePath() {
		return profilePath;
	}

	public void setProfilePath(String profilePath) {
		this.profilePath = profilePath;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserTel() {
		return userTel;
	}

	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public String getUserNote() {
		return userNote;
	}
	
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public CategoryEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryEntity category) {
		this.category = category;
	}

		
	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public static Finder<Long, UserEntity> find = new Finder<Long, UserEntity>(UserEntity.class);
	
}
