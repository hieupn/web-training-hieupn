package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.avaje.ebean.Model;

@Entity
@Table(name = "category_table")

public class CategoryEntity extends Model {
	
	@Id 
	private Integer			categoryId;
	private String			categoryName;
	private String			categoryNote;
	
	@OneToMany(mappedBy="category", targetEntity=UserEntity.class)
	public List<UserEntity> joinUsers;
	
	public CategoryEntity() {
		super();
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryNote() {
		return categoryNote;
	}

	public void setCategoryNote(String categoryNote) {
		this.categoryNote = categoryNote;
	}

	public List<UserEntity> getJoinUsers() {
		return joinUsers;
	}

	public void setJoinUsers(List<UserEntity> joinUsers) {
		this.joinUsers = joinUsers;
	}
	
	public static Finder<Long, CategoryEntity> find = new Finder<Long, CategoryEntity>(CategoryEntity.class);
	

}
