package models;

import java.util.ArrayList;
import java.util.List;

import models.UserModel;

public class CategoryModel{

	public Integer			categoryId;

	public String			categoryName;

	public String			categoryNote;

	public List<UserModel>	joinUsers	= new ArrayList<>();

	public CategoryModel() {
		super();
	}
	
	
	public CategoryModel(CategoryEntity db) {
		super();
		this.categoryId = db.getCategoryId();
		this.categoryName = db.getCategoryName();
		this.categoryNote = db.getCategoryNote();
		for(UserEntity entity : db.getJoinUsers()) {
			this.joinUsers.add(new UserModel(entity,true));
		}
	}
	

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryNote() {
		return categoryNote;
	}

	public void setCategoryNote(String categoryNote) {
		this.categoryNote = categoryNote;
	}

	public List<UserModel> getJoinUsers() {
		return joinUsers;
	}

	public void setJoinUsers(List<UserModel> joinUsers) {
		this.joinUsers = joinUsers;
	}

}
