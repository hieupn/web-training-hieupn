package models;

public class UserModel{

	public Integer	userId;

	public String	profilePath;

	public String	userName;

	public String	userTel;

	public String	userMail;

	public CategoryModel	userCategory;
	
	public Integer 			categoryId;

	public String	userNote;
	public String	userPassword;

	public UserModel(){
		super();
	}
	
	public UserModel(UserEntity db){
		super();
		this.userId = db.getUserId();
		this.userCategory = new CategoryModel(db.getCategory());
		this.categoryId = db.getCategory().getCategoryId();
		this.userName = db.getUserName();
		this.userTel = db.getUserTel();
		this.userMail = db.getUserMail();
		this.userNote = db.getUserNote();
		this.userPassword = db.getUserPassword();
		if(db.getProfilePath()==null||db.getProfilePath()=="") 
			this.profilePath = "default_avatar2.png";
		else 		
			this.profilePath = db.getProfilePath();
	}
	public UserModel(UserEntity db, boolean isSimple){
		super();
		this.userId = db.getUserId();
		this.userName = db.getUserName();
		if(db.getProfilePath()==(null)||db.getProfilePath()=="") 
			this.profilePath = "default_avatar2.png";
		else
			this.profilePath = db.getProfilePath();
	}
	
	

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getProfilePath() {
		return profilePath;
	}

	public void setProfilePath(String profilePath) {
		this.profilePath = profilePath;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserTel() {
		return userTel;
	}

	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public CategoryModel getUserCategory() {
		return userCategory;
	}

	public void setUserCategory(CategoryModel userCategory) {
		this.userCategory = userCategory;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

}
