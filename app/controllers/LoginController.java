package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.annotation.Transactional;

import akka.actor.dsl.Inbox.Query;
import models.CategoryEntity;
import models.CategoryModel;
import models.LoginEntity;
import models.LoginModel;
import models.UserEntity;
import models.UserModel;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.login;


public class LoginController extends Controller {
	

	@Transactional
	public Result login(){
		
		List<LoginModel> lgList = new ArrayList<LoginModel>();
		List<LoginEntity> entities = LoginEntity.find.all();		
		
		for(LoginEntity db : entities){
			lgList.add(new LoginModel(db));
		}
		

		for(LoginEntity db: entities) {
			lgList.add(new LoginModel(db));
		}
		
		return ok(login.render(lgList));
		
	}
	public Result check() {
		
	Map<String, String[]> values = request().body().asFormUrlEncoded();
		String Idform = values.get("loginId")[0];
		String pass = values.get("password")[0];
		try {
			Integer  Id = Integer.parseInt(Idform);
			LoginEntity db = Ebean.find(LoginEntity.class, Id);
			
			if(db.getUserPassword().equals(pass)) {
				return redirect("/category");
			}
			else		
			return redirect("/login");
		
		}catch (Exception ex) {
			ex.printStackTrace();
			return redirect("/login");
		}

	}
	
	
}
