package controllers;

import java.io.File;

import play.mvc.Controller;
import play.mvc.Result;
import services.define.TrainingConstants;

public class ImageControllers extends Controller {
public Result image(String image){
		
		File file = new File(TrainingConstants.FILE_PATH + image);
		return ok(file);
	}
}
