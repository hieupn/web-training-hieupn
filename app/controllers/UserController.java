package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.CategoryEntity;
import models.CategoryModel;
import models.UserEntity;
import models.UserModel;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import services.define.TrainingConstants;
import services.utils.Utils;
import views.html.user;
import views.html.user_form;

/**
 * This controller contains an action to handle HTTP requests to the
 * application's home page.
 */
public class UserController extends Controller{

	/**
	 * An action that renders an HTML page with a welcome message. The
	 * configuration in the <code>routes</code> file means that this method will
	 * be called when the application receives a <code>GET</code> request with a
	 * path of <code>/</code>.
	 */
	
	@Inject
	FormFactory formFactory;
	public Result user(){
		List<UserModel> usrList = new ArrayList<UserModel>();
		List<UserEntity> entities = UserEntity.find.all();		
		
		for(UserEntity db : entities){
			usrList.add(new UserModel(db));	
		}
		return ok(user.render(usrList));
	}

	public Result userForm(){
		
			Form<UserModel> userForm = Form.form(UserModel.class);		
			UserModel userModel = userForm.form(UserModel.class).bindFromRequest().get();
				
				if(userModel.getUserId().equals(0)){
					userModel = new UserModel();
					userModel.setUserId(0);
					userModel.setUserCategory(new CategoryModel());
				}else {		
					UserEntity  db = Ebean.find(UserEntity.class, userModel.getUserId());
					userModel = new UserModel(db);
				}
			
			Query<CategoryEntity> query =  Ebean.createQuery(CategoryEntity.class);
			
			List<CategoryModel> listCateModel = new ArrayList<>();
			for(CategoryEntity entity : query.findList()) {
				listCateModel.add(new CategoryModel(entity));
			}
		
		return ok(user_form.render(userModel,listCateModel));
	}
	
	
	public Result update() {
		Form<UserModel> userFrom = Form.form(UserModel.class);
		UserModel userModel = userFrom.form(UserModel.class).bindFromRequest().get();
		
//		Upload images
		MultipartFormData<File> mFile = request().body().asMultipartFormData();
		FilePart<File> imageProfile = mFile.getFile("picture");

		try{
			String fileName = Utils.makeUploadFileName(imageProfile);
			if(fileName.equals("###")) {
				userModel.profilePath ="###";
			}
			else {
				userModel.profilePath = "/assets/images/Avatar/"+fileName;
			}

			OutputStream out = null;
			FileInputStream fis = null;
			try{
				out = new FileOutputStream(new File(TrainingConstants.FILE_PATH + fileName));
				fis = new FileInputStream(imageProfile.getFile());

				int read = 0;
				final byte[] bytes = new byte[1024];

				while((read = fis.read(bytes)) != -1){
					out.write(bytes, 0, read);
				}
			}catch(Exception fne){
				fne.printStackTrace();
			}finally{
				if(out != null){
					out.close();
				}

				if(fis != null){
					fis.close();
				}
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
//		Het phan upload Images
		
		CategoryEntity ct = Ebean.find(CategoryEntity.class, userModel.getCategoryId());
	
		if (userModel.getUserId() == 0) {
			UserEntity db = new UserEntity();
			db.setUserName(userModel.getUserName());
			db.setUserTel(userModel.getUserTel());
			db.setUserMail(userModel.getUserMail());
			db.setCategory(ct);
			db.setUserNote(userModel.getUserNote());
//			db.setUserPassword(userModel.getUserPassword());
			if(userModel.getProfilePath().equals("###")) {}
			else db.setProfilePath(userModel.getProfilePath());
			db.save();
		} else {
			UserEntity db = Ebean.find(UserEntity.class, userModel.getUserId());
			db.setUserName(userModel.getUserName());
			db.setUserTel(userModel.getUserTel());
			db.setUserMail(userModel.getUserMail());
			db.setCategory(ct);
			db.setUserNote(userModel.getUserNote());
//			db.setUserPassword(userModel.getUserPassword());
			if(userModel.getProfilePath().equals("###")) {}
			else db.setProfilePath(userModel.getProfilePath());
			db.save();
		}

		ObjectNode result = Json.newObject();
		result.put("STATUS", "OK");

		return ok(result);
	}
		
	public Result delete() {
		Form<UserModel> userFrom = Form.form(UserModel.class);
		UserModel userModel = userFrom.form(UserModel.class).bindFromRequest().get();

		UserEntity db = Ebean.find(UserEntity.class, userModel.getUserId());
		db.delete();

		ObjectNode result = Json.newObject();
		result.put("STATUS", "OK");
		return ok(result);
	}
	
	public Result upload() throws IOException {		
// Upload ảnh theo Cách 1	
		try {
			File file = request().body().asRaw().asFile();
			File newFile = new File("/home/hieupn/workspace/web-training-hieupn/public/images/Avatar/" + Calendar.getInstance().getTimeInMillis());
			file.renameTo(newFile);
			file.createNewFile();
			return ok(newFile.getName());					
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ok("Upload fail!");		
	}	
}
