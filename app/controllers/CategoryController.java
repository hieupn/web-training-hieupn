package controllers;

import java.util.ArrayList;
import java.util.List;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import com.avaje.ebean.annotation.Transactional;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.CategoryEntity;
import models.CategoryModel;
import models.UserEntity;
import models.UserModel;

import play.data.Form;
import play.mvc.*;
import views.html.category;
import views.html.category_form;
import scala.util.parsing.json.JSONObject;
import play.libs.Json;

/**
 * This controller contains an action to handle HTTP requests to the
 * application's home page.
 */
public class CategoryController extends Controller{

	/**
	 * An action that renders an HTML page with a welcome message. The
	 * configuration in the <code>routes</code> file means that this method will
	 * be called when the application receives a <code>GET</code> request with a
	 * path of <code>/</code>.
	 */
	@Transactional
	public Result category(){
		List<CategoryModel> ctList = new ArrayList<CategoryModel>();

		List<CategoryEntity> entities = CategoryEntity.find.all();		
		
		for(CategoryEntity db : entities){
			ctList.add(new CategoryModel(db));
		
		}
		return ok(category.render(ctList));
	}

	public Result categoryForm(){
			
		Form<CategoryModel> categoryForm = Form.form(CategoryModel.class);
		
		CategoryModel categoryModel = categoryForm.form(CategoryModel.class).bindFromRequest().get();
		
		if(categoryModel.getCategoryId().equals(0)){
			categoryModel = new CategoryModel();
			categoryModel.setCategoryId(0);
		}else {
			
			CategoryEntity  db = Ebean.find(CategoryEntity.class, categoryModel.getCategoryId());
			categoryModel = new CategoryModel();
			categoryModel.setCategoryName(db.getCategoryName());
			categoryModel.setCategoryNote(db.getCategoryNote());
			categoryModel.setCategoryId(db.getCategoryId());
			
		}
			
		return ok(category_form.render(categoryModel));
	}
	
	
	@Transactional
	public Result update(){
		
		Form<CategoryModel> categoryForm = Form.form(CategoryModel.class);
		CategoryModel categoryModel = categoryForm.form(CategoryModel.class).bindFromRequest().get();
		
		if(categoryModel.getCategoryId() == 0){
			CategoryEntity db = new CategoryEntity();
			db.setCategoryName(categoryModel.getCategoryName());
			db.setCategoryNote(categoryModel.getCategoryNote());
			db.save();
		}else {
			CategoryEntity  db = Ebean.find(CategoryEntity.class, categoryModel.getCategoryId());
			db.setCategoryName(categoryModel.getCategoryName());
			db.setCategoryNote(categoryModel.getCategoryNote());
			db.save();
		}
		
		ObjectNode result = Json.newObject();
		result.put("STATUS", "OK");
	
		
		return ok(result);
	}
	
	@Transactional
	public Result delete() {
		Form<CategoryModel> categoryForm = Form.form(CategoryModel.class);
		CategoryModel categoryModel = categoryForm.form(CategoryModel.class).bindFromRequest().get();
		
		CategoryEntity  db = Ebean.find(CategoryEntity.class, categoryModel.getCategoryId());
		db.delete();
		
		ObjectNode result = Json.newObject();
		result.put("STATUS", "OK");
		return ok(result);
	}
	
}
